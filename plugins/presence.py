import datetime
import os
import yaml

def TIMESTAMP() :
    return datetime.datetime.utcnow().strftime("%H:%M:%S %d-%m-%Y (UTC)")

class Mailer:
    messages_to_send = None
    storagelocation = None
    storagefile = None

    def __init__(self, storagelocation = "./"):
        self.messages_to_send = {}
        self.storagefile = storagelocation + "/messages.yml"
        if os.path.isfile(self.storagefile):
            self.load_messages()

    def get_messages(self, nick):
        nick = nick.upper()
        if nick in self.messages_to_send.keys():
            messages = self.messages_to_send[nick]
            del self.messages_to_send[nick]
            return messages
        else:
            return None

    def new_message(self, nick, sender, message):
        nick = nick.upper()
        if nick in self.messages_to_send.keys():
            self.messages_to_send[nick].append([sender, TIMESTAMP(), message])
        else:
            self.messages_to_send[nick] = []
            self.messages_to_send[nick].append([sender, TIMESTAMP(), message])

    def load_messages(self):
        print("Loading messages.")
        with open(self.storagefile, "r") as f:
            obj = yaml.safe_load(f)
            if not obj is None:
                self.messages_to_send = obj
        os.remove(self.storagefile)

    def save_messages(self):
        print("Saving messages...")
        with open(self.storagefile, "w") as fob:
            fob.write(yaml.dump(self.messages_to_send, explicit_start=True))
        return


class precense:
    bot = None;
    mailer = None;
    storagefile = None;
    nick_names = dict()

    def __init__(self, parent_bot = None):

        self.bot = parent_bot
        self.mailer = Mailer(self.bot.storagelocation)
        self.storagefile = self.bot.storagelocation + "/presences.yml"
        self.loadprecenses()

    def loadprecenses(self):
        if not os.path.isfile(self.storagefile):
            return
        print("Loading precenses.")
        with open(self.storagefile, "r") as fob:
            self.nick_names = yaml.safe_load(fob)
        os.remove(self.storagefile)

        if self.nick_names is None:
            self.nick_names = {}

    def saveprecenses(self):
        print("Saving precenses...")

        for key in self.nick_names.items():
            try:
                if self.nick_names[key] == 0:
                    del self.nick_names[key]
            except:
                pass

        with open(self.storagefile, "w") as fob:
            fob.write(yaml.dump(self.nick_names))

    def check_nick_messages(self, nick):
        messages = self.mailer.get_messages(nick)

        if messages != None:
            for message in messages:
                self.bot.send_channel_msg("%s, %s left you a message at %s : %s" % (nick, message[0], message[1], message[2]))

    def process_privmsg(self, sender_nick, tokens):
        self.check_nick_messages(sender_nick)
        if self.bot.config.nick in tokens[0]:
            tokens = tokens[1:len(tokens)]

            if tokens[0] == "tell" and len(tokens) >= 2:
                nick = tokens[1]
                self.mailer.new_message(nick, sender_nick, " ".join(tokens[2 : len(tokens)]))
                self.bot.send_channel_msg("Okay, %s, I'll pass your message along!" % sender_nick)

            elif tokens[0] == "seen" and len(tokens) >= 2:
                nick = tokens[1]

                if nick in self.nick_names.keys():
                    self.bot.send_channel_msg("I saw %s at %s (%s)." % (nick, self.nick_names[nick][1], self.nick_names[nick][0]))
                else:
                    self.bot.send_channel_msg("Sorry, I haven't seen %s." % nick)
        else:
            self.add_nick(sender_nick, " ".join(tokens))


    def add_nick(self, nickname, reason, overwrite = True):
        if nickname[0] == "@":
            nickname[1:]
        if nickname in self.nick_names:
            if overwrite == True:
                self.nick_names[nickname] = [reason, TIMESTAMP()]
        else:
           self.nick_names[nickname] = [reason, TIMESTAMP()]

    def process_join(self, sender_nick, token):
        self.add_nick(sender_nick, "JOIN")
        join_nick = token[0].split("!")[0].replace(":", "")
        #self.check_nick_messages(join_nick)
        print("total nicks : %s" % len(self.nick_names))

    def process_names(self, sender_nick, tokens):
        tokens[3] = tokens[3][1:]
        for token in tokens[3:len(tokens)]:
            self.add_nick(token, "IN CHANNEL", False)
            #self.check_nick_messages(token)

        print("total nicks : %s" % len(self.nick_names))

    def process_part(self, sender_nick, token):
        self.add_nick(sender_nick, "PART")

    def process_quit(self, sender_nick, token):
        self.add_nick(sender_nick, "QUIT")

    def save(self):
        self.saveprecenses()
        self.mailer.save_messages()


def registerplugin(bot):
    plugin = precense(bot)
    bot.registercallback("JOIN", plugin.process_join )
    bot.registercallback("PRIVMSG", plugin.process_privmsg )
    bot.registercallback("PART", plugin.process_part )
    bot.registercallback("QUIT", plugin.process_quit )
    bot.registercallback("353", plugin.process_names)
    bot.registercallback("SHUTDOWN", plugin.save)

def nameofplugin():
    return "Presence"
