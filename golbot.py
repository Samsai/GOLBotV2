#! /usr/bin/python3
#
#  This program is free software; you can redistribute it and/or modify
#  it under the terms of the GNU General Public License as published by
#  the Free Software Foundation; either version 2 of the License, or
#  (at your option) any later version.
#
#  This program is distributed in the hope that it will be useful,
#  but WITHOUT ANY WARRANTY; without even the implied warranty of
#  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
#  GNU General Public License for more details.
#
#  You should have received a copy of the GNU General Public License
#  along with this program; if not, write to the Free Software
#  Foundation, Inc., 51 Franklin Street, Fifth Floor, Boston,
#  MA 02110-1301, USA.
#

import sys
import datetime
import socket
import string
import importlib
import pathlib
import time
import asyncio

import Config
from ErrorReporting import print_exception



class IRCPRotocol(asyncio.Protocol):
    def __init__(self, processor, loop):
        self.loop = loop
        self.processor = processor

    def connection_made(self, transport):
        self.transport = transport
        self.processor.connection_made(self)

    def send_msg(self, data):
        data += "\r\n"
        self.transport.write(data.encode())

    readbuffer = str()
    def data_received(self, data):
        temp = None
        if data is not None:
            self.readbuffer += format(data.decode())
            temp = str.split(self.readbuffer, "\n")
            if len(temp)>1:
                self.readbuffer=temp.pop()

        q = self.processor.process_line(temp)

        self.loop.create_task(q)

    def connection_lost(self, exc):
        self.processor.connection_lost()

    def send_nick(self, nick):
        self.send_msg("NICK " + nick)

    def send_user(self, ident, server, realname):
        self.send_msg("USER %s %s bla :%s" % (ident, server, realname))

    def send_join(self, channel):
        self.send_msg("JOIN #" + channel)

    def send_channel_msg(self, channel, msg):
        msg = "PRIVMSG #" + channel + " :" + msg
        self.send_msg(msg)


class GOLBot():
    configlocation = None
    storagelocation = None
    irc = None
    coro = None
    protocol = None
    transport = None
    loop = None
    config = None
    feeder = None

    plugins = dict()

    def __init__(self, configlocation = "./", storagelocation = "./"):
        try:
            self.storagelocation = storagelocation
            self.configlocation = configlocation
            self.loop = asyncio.get_event_loop()
            self.config = Config.ConfigHandler(configlocation)
            self.loadplugins()
       except:
            print_exception("There was an error initializing the bot.")
            sys.exit(1)
    
    def connect(self):
        while True:
            try:
                self.irc = socket.socket()
                self.irc.settimeout(200)
                self.irc.connect((self.server, 6667))
                self.send_msg("NICK %s\r\n" % self.nick)
                self.send_msg("USER %s %s bla :%s\r\n" % (self.ident, self.server, self.realname))
                
                self.send_msg("PRIVMSG NickServ :identify %s %s\r\n" % (self.nick, self.password));
                
                time.sleep(5)

                self.send_msg("JOIN %s\r\n" % self.channel)
                print("Connection established")
                break
            except:
                print("Failed to establish connection... Retrying.")
                e = sys.exc_info()
                print(e)
                time.sleep(1)
    

    def registercallback(self, event, cb):
        try :
            cbs = self.plugins[event]
        except:
            cbs = list()

        cbs.append(cb)
        self.plugins[event] = cbs


    def loadplugins(self):
        self.plugins["CONNECTED"] = list()
        self.plugins["JOIN"] = list()
        self.plugins["PART"] = list()
        self.plugins["PRIVMSG"] = list()
        self.plugins["QUIT"] = list()

        cwd = pathlib.Path('plugins').resolve()
        for path in cwd.iterdir():
            if not path.stem.startswith('__'):
                try:
                    module = importlib.import_module(f'plugins.{path.stem}')
                    print("Found plugin : %s" % module.nameofplugin())
                    module.registerplugin(self)
                except:
                    print_exception("Error loading plugin %s" % path.stem)

    def quitPlugins(self):
        plugins = self.plugins["SHUTDOWN"]
        if plugins is not None:
                for function in plugins:
                    function()
    def signalConnectedPlugins(self):
        plugins = self.plugins["CONNECTED"]
        if plugins is not None:
                for function in plugins:
                    function()


    def checkPlugins(self, event, nick, text):
        retval = None

        plugins = self.plugins[event]
        if plugins is not None:
                for function in plugins:
                    try:
                        function(nick, text)
                    except:
                        print_exception("Exception while handling event %s and callback %s" %(event, function))

    def connection_made(self, protocol):
        self.protocol = protocol
        protocol.send_nick(self.config.nick)
        protocol.send_user(self.config.ident, self.config.server, self.config.realname)
        protocol.send_join(self.config.channel)
        self.signalConnectedPlugins()

    def connect(self):
        self.coro = self.loop.create_connection(lambda: IRCPRotocol(self, self.loop), self.config.server, 6667)
        self.loop.run_until_complete(self.coro)

    def send_msg(self, msg):
        try:
            if not self.protocol is None:
                self.protocol.send_msg(msg)
            print("Sent:" + msg)
        except:
            print_exception("Failed to send message!")

    def send_channel_msg(self, msg):
        try:
            if not self.protocol is None:
                self.protocol.send_channel_msg(self.config.channel, msg)
        except:
            print_exception("Failed to send message!")

    def handle_command_message(self, line):
        retval = None
        if line.startswith("PING "):
            retval = "PONG %s" % line.split(":")[1];
        elif line.startswith("ERROR "):
            time.sleep(30)
            self.irc.close()
            self.connect()
            print("ERR:" + line)
            retval = None

        return retval

    def run(self):
        self.connect()
        self.loop.run_forever()

    async def process_line(self, text):
        try:
            if text is not None:
                for line in text:
                    line = line.strip()

                    if len(line) > 0:
                        if line[0] != ":":
                            result = self.handle_command_message(line)
                            if result is not None:
                                self.send_msg(result)
                        else:
                            tokens = line.split()
                            origin = tokens[0]
                            sender_nick = origin.split("!")[0].replace(":", "")
                            event = tokens[1]
                            #we don't care about private messages
                            if self.config.channel in tokens[2]:
                                try:
                                    print("received : " + event)
                                    if event == "PRIVMSG":
                                        # first parameter, minus the :
                                        tokens[3] = tokens[3][1:]
                                        self.checkPlugins(event, sender_nick, tokens[3:len(tokens)])

                                    elif event == "JOIN":
                                        # first parameter, minus the :
                                        #tokens[2] = tokens[2][1:]
                                        self.checkPlugins(event, sender_nick, tokens[2:len(tokens)])

                                    elif event == "PART":
                                        # first parameter, minus the :
                                        #tokens[2] = tokens[2][1:]
                                        self.checkPlugins(event, sender_nick, tokens[2:len(tokens)])
                                except:
                                    print_exception("Woops, something went wrong!")
                            else:
                                ## with the exception of a few......
                                ### names
                                if event == "353":
                                    self.checkPlugins(event, sender_nick, tokens[2:len(tokens)])
                                ### quits are global messages it seems.
                                elif event == "QUIT":
                                    self.checkPlugins(event, sender_nick, tokens[2:len(tokens)])
        except:
            print_exception("process_line")

if __name__ == '__main__':
    try:
        golbot = GOLBot("/etc/golbot", "/var/golbot")
        golbot.run()
    except :
        print("Bot quitting... Please wait.")
        golbot.quitPlugins()


