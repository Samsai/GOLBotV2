import os
import yaml

#NICK = "GOLBotV3"
#SERVER = "chat.freenode.net"
#CHANNEL = "#golbottest"
#RSS = "https://www.gamingonlinux.com/article_rss.php"
#RSS_DELAY = 180
#IDENT = "daGOLBOT"
#REALNAME = "daGOLBOT"

class ConfigHandler:
    configfile = None
    configs = None

    def __init__(self, configlocation):
        self.configfile = configlocation + "/config.yml"
        with open(self.configfile) as f:
            yamlconfig = yaml.safe_load(f)
            self.__dict__.update(yamlconfig)

    def get_configs(self):
        return self.yamlconfig

    def print_configs(self):
        print (yaml.dump(self.yamlconfig))
